<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Auxiliaweb.AuxiliawebMediaplayer',
            'Mediaplayer',
            'Mediaplayer'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'auxiliaweb_mediaplayer',
            'Configuration/TypoScript',
            'Mediaplayer'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
            'tx_auxiliawebmediaplayer_domain_model_mediaitem',
            'EXT:auxiliaweb_mediaplayer/Resources/Private/Language/locallang_csh_tx_auxiliawebmediaplayer_domain_model_mediaitem.xlf'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_auxiliawebmediaplayer_domain_model_mediaitem'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
            'auxiliawebmediaplayer_mediaplayer',
            'FILE:EXT:auxiliaweb_mediaplayer/Configuration/FlexForms/flexform.xml'
        );
    }
);
