<?php

namespace Auxiliaweb\AuxiliawebMediaplayer\Domain\Repository;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * The repository for Mediaitems
 */
class MediaitemRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    /**
     * @param $uidList - 1,2,3
     * @return array
     */
    public function findByUidlist($uidList) {
        $uidList = explode(',', $uidList);

        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->in('uid', $uidList),
                $query->equals('hidden', 0),
                $query->equals('deleted', 0)
            )
        );
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        $result = $query->execute();

        return $this->orderByKey($result, $uidList);
    }

    /**
     * @param $storagePid
     * @return array
     */
    public function findByPid($storagePid) {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->equals('pid', $storagePid),
                $query->equals('hidden', 0),
                $query->equals('deleted', 0)
            )
        );
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        return $query->execute();
    }

    /**
     * @param $result
     * @param $uidList
     * @return array
     */
    protected function orderByKey($result, $uidList) {
        $orderedResult = [];
        foreach ($uidList as $uid) {
            foreach ($result as $entry) {
                if ($entry->getUid() == $uid) {
                    array_push($orderedResult, $entry);
                    break;
                }
            }
        }
        return $orderedResult;
    }
}
