<?php

namespace Auxiliaweb\AuxiliawebMediaplayer\Domain\Model;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * Mediaitem
 */
class Mediaitem extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * interpreter
     *
     * @var string
     */
    protected $interpreter = '';

    /**
     * releaseDate
     *
     * @var string
     */
    protected $releaseDate = '';

    /**
     * mediaelement
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $mediaelement;

    /**
     * cover
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $cover;

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getInterpreter(): string {
        return $this->interpreter;
    }

    /**
     * @param string $interpreter
     */
    public function setInterpreter($interpreter) {
        $this->interpreter = $interpreter;
    }

    /**
     * @return string
     */
    public function getReleaseDate(): string {
        return $this->releaseDate;
    }

    /**
     * @param string $releaseDate
     */
    public function setReleaseDate($releaseDate) {
        $this->releaseDate = $releaseDate;
    }

    /**
     * Get the mediaelement
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getMediaelement() {
        return $this->mediaelement;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $mediaelement
     */
    public function setMediaelement($mediaelement) {
        $this->mediaelement = $mediaelement;
    }

    /**
     * Get the cover
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getCover() {
        return $this->cover;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $cover
     */
    public function setCover($cover) {
        $this->cover = $cover;
    }


}
