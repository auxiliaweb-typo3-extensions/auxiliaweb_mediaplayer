<?php

namespace Auxiliaweb\AuxiliawebMediaplayer\Service;

use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * MediaplayerService
 */
class MediaplayerService {

    /**
     * mediaitemRepository
     *
     * @var \Auxiliaweb\AuxiliawebMediaplayer\Domain\Repository\MediaitemRepository
     * @inject
     */
    protected $mediaitemRepository;

    /**
     * mediaplayerHelper
     *
     * @var \Auxiliaweb\AuxiliawebMediaplayer\Utility\MediaplayerHelper
     * @inject
     */
    protected $mediaplayerHelper;

    /**
     * @var \TYPO3\CMS\Core\Collection\RecordCollectionRepository
     * @inject
     */
    protected $collectionRepository;

    /**
     * Return array of selected mediaitems
     *
     * @return array
     */
    public function getMediaitems() {
        $mediaitems = array();
        if ($this->mediaplayerHelper->getSource() === 'singleMediaitems') {
            $mediaitems = $this->mediaitemRepository->findByUidlist($this->mediaplayerHelper->getMediaitems());
        } else {
            foreach ($this->collectionRepository->findByTableName('tx_auxiliawebmediaplayer_domain_model_mediaitem') as $collection) {
                $collection->loadContents();

                foreach ($collection as $collectionItem) {
                    $mediaitems[] = $this->mediaitemRepository->findByUidlist($collectionItem['uid'])[0];
                }
            }
        }

        $mediaitems = $this->group($mediaitems);
        $mediaitems = $this->sort($mediaitems);
        $mediaitems = $this->limit($mediaitems);

        return $mediaitems;
    }

    /**
     * Create mediaitems
     *
     * @param $path
     * @param $recursive
     * @param $storagePid
     * @param $systemCollectionUid
     */
    public function indexMediaitems($path, $recursive, $storagePid, $systemCollectionUid) {
        $mediafiles = $this->getMediafiles($path, $recursive);
        $mediaitems = $this->mediaitemRepository->findByPid($storagePid);

        $this->insertNewMediaitems($mediafiles, $mediaitems);
    }

    /**
     * @param $path
     * @param $recursive
     * @return array
     */
    private function getMediafiles($path, $recursive) {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $parentFolder = $resourceFactory->retrieveFileOrFolderObject($path);

        $mediafiles = [];
        $mediafilesUnfiltered = $parentFolder->getFiles(0, 0, 0, $recursive === '1', '', false);
        foreach ($mediafilesUnfiltered as $mediafile) {
            if ($mediafile->getProperties()['extension'] === 'mp3' || $mediafile->getProperties()['extension'] === 'wav') {
                $mediafiles[] = $mediafile;
            }
        }

        return $mediafiles;
    }


    /**
     * @param $mediafiles
     * @param $mediaitems
     */
    private function insertNewMediaitems($mediafiles, $mediaitems) {
        $this->mediaplayerHelper->dump($mediafiles);
        $this->mediaplayerHelper->dump($mediaitems);

        foreach ($mediafiles as $mediafile) {

        }
    }

    /**
     * Return array of generated groups
     *
     * @param $mediaitems \Auxiliaweb\AuxiliawebMediaplayer\Domain\Model\Mediaitem
     * @return array
     */
    private function group($mediaitems) {
        $groupedMediaitems = array();
        $groupBy = $this->mediaplayerHelper->getGroupBy();
        foreach ($mediaitems as $mediaitem) {
            if ($groupBy === 'releaseDateAsc' || $groupBy === 'releaseDateDesc') {
                $groupedMediaitems[$mediaitem->getReleaseDate()][] = $mediaitem;
            } else if ($groupBy === 'releaseDateYearAsc' || $groupBy === 'releaseDateYearDesc') {
                $year = date('Y', $mediaitem->getReleaseDate());
                $groupedMediaitems[$year][] = $mediaitem;
            } else if ($groupBy === 'releaseDateYearAndMonthAsc' || $groupBy === 'releaseDateYearAndMonthDesc') {
                $year = date('Ym', $mediaitem->getReleaseDate());
                $groupedMediaitems[$year][] = $mediaitem;
            } else if ($groupBy === 'interpreterAsc' || $groupBy === 'interpreterDesc') {
                $groupedMediaitems[$mediaitem->getInterpreter()][] = $mediaitem;
            } else {
                $groupedMediaitems[$this->mediaplayerHelper->getGroupBy()][] = $mediaitem;
            }
        }

        //sort groups
        if ($groupBy !== 'noGrouping') {
            if (substr($groupBy, strlen($groupBy) - 3, 3) == 'Asc') {
                ksort($groupedMediaitems, SORT_NATURAL | SORT_FLAG_CASE);
            } else {
                krsort($groupedMediaitems, SORT_NATURAL | SORT_FLAG_CASE);
            }
        }

        //replace keys with real text
        if ($groupBy === 'releaseDateYearAndMonthAsc' || $groupBy === 'releaseDateYearAndMonthDesc') {
            $sortedGroupsAndMediaitems = array();

            foreach ($groupedMediaitems as $key => $group) {
                $year = substr($key, 0, 4);
                $month = substr($key, 4, 2);
                $realName = LocalizationUtility::translate('tx_auxiliawebmediaplayer_mediaplayer.' . $month, 'auxiliaweb_mediaplayer') . ' ' . $year;

                $sortedGroupsAndMediaitems[$realName] = $group;
            }

            return $sortedGroupsAndMediaitems;
        }

        return $groupedMediaitems;
    }

    /**
     * Sort mediaitems
     *
     * @param $mediaitems \Auxiliaweb\AuxiliawebMediaplayer\Domain\Model\Mediaitem
     * @return array
     */
    private function sort($groupedMediaitems) {
        $orderBy = $this->mediaplayerHelper->getOrderBy();
        $orderByDirection = $this->mediaplayerHelper->getOrderByDirection();
        foreach ($groupedMediaitems as $key => $group) {
            if ($orderBy === 'releaseDate') {
                if ($orderByDirection === 'ascending') {
                    usort($group, function ($a, $b) {
                        return strcmp($a->getReleaseDate(), $b->getReleaseDate());
                    });
                } else {
                    usort($group, function ($a, $b) {
                        return strcmp($b->getReleaseDate(), $a->getReleaseDate());
                    });
                }
            } else if ($orderBy === 'title') {
                if ($orderByDirection === 'ascending') {
                    usort($group, function ($a, $b) {
                        return strcmp($a->getTitle(), $b->getTitle());
                    });
                } else {
                    usort($group, function ($a, $b) {
                        return strcmp($b->getTitle(), $a->getTitle());
                    });
                }
            } else if ($orderBy === 'interpreter') {
                if ($orderByDirection === 'ascending') {
                    usort($group, function ($a, $b) {
                        return strcmp($a->getInterpreter(), $b->getInterpreter());
                    });
                } else {
                    usort($group, function ($a, $b) {
                        return strcmp($b->getInterpreter(), $a->getInterpreter());
                    });
                }
            }

            $groupedMediaitems[$key] = $group;
        }

        return $groupedMediaitems;
    }

    /**
     * Limit mediaitems
     *
     * @param $mediaitems \Auxiliaweb\AuxiliawebMediaplayer\Domain\Model\Mediaitem
     * @return array
     */
    private function limit($groupedMediaitems) {
        $limit = $this->mediaplayerHelper->getLimit();
        foreach ($groupedMediaitems as $key => $group) {
            $size = sizeof($groupedMediaitems[$key]);

            if ($size > $limit) {
                array_splice($group, $limit, $size - $limit);
            }

            $groupedMediaitems[$key] = $group;
        }

        return $groupedMediaitems;
    }
}
