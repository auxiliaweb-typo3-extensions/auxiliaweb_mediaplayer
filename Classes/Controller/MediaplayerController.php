<?php

namespace Auxiliaweb\AuxiliawebMediaplayer\Controller;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * MediaplayerController
 */
class MediaplayerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
     * mediaplayerHelper
     *
     * @var \Auxiliaweb\AuxiliawebMediaplayer\Utility\MediaplayerHelper
     * @inject
     */
    protected $mediaplayerHelper;

    /**
     * mediaplayerService
     *
     * @var \Auxiliaweb\AuxiliawebMediaplayer\Service\MediaplayerService
     * @inject
     */
    protected $mediaplayerService;

    /**
     * action show
     *
     * @return void
     */
    public function showAction() {
        $configuration = new \stdClass();

        // General Settings
        $configuration->mediatype = $this->mediaplayerHelper->getMediatype();
        $configuration->source = $this->mediaplayerHelper->getSource();

        // Template Settings
        $configuration->oneMediaplayerPerMediaitem = $this->mediaplayerHelper->getOneMediaplayerPerMediaitem();
        $configuration->hideOptions = $this->mediaplayerHelper->getHideOptions();
        $configuration->hideCover = $this->mediaplayerHelper->getHideCover();
        $configuration->maxWidth = $this->mediaplayerHelper->getMaxWidth();
        $configuration->orientation = $this->mediaplayerHelper->getOrientation();
        $configuration->hidePlaylist = $this->mediaplayerHelper->getHidePlaylist();

        // Other Settings
        $configuration->hideGroupHeadlines = $this->mediaplayerHelper->getHideGroupHeadlines();
        $configuration->groupBy = $this->mediaplayerHelper->getGroupBy();
        $configuration->orderBy = $this->mediaplayerHelper->getOrderBy();
        $configuration->orderByDirection = $this->mediaplayerHelper->getOrderByDirection();
        $configuration->limit = $this->mediaplayerHelper->getLimit();

        $configuration->audioCover = $this->mediaplayerHelper->getAudioCover();
        $configuration->videoCover = $this->mediaplayerHelper->getVideoCover();

        $this->view->assign('configuration', $configuration);

        $mediaitems = $this->mediaplayerService->getMediaitems();
        $this->view->assign('mediaitems', $mediaitems);

        $uid = $this->configurationManager->getContentObject()->data['uid'];
        $this->view->assign('uid', $uid);
    }
}
