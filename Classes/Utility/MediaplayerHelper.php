<?php
namespace Auxiliaweb\AuxiliawebMediaplayer\Utility;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * MediaplayerHelper
 */
class MediaplayerHelper {

    /**
     * Settings
     *
     * @var array
     */
    protected $settings = [];

    /**
     * Returns whether each item is display by one player
     *
     * @return bool
     */
    public function getOneMediaplayerPerMediaitem() {
        return $this->settings['settings']['oneMediaplayerPerMediaitem'] == null ? false : $this->settings['settings']['oneMediaplayerPerMediaitem'];
    }

    /**
     * Return array of selected mediaitems
     *
     * @return array
     */
    public function getMediaitems() {
        return $this->settings['settings']['mediaitems'];
    }

    /**
     * Return array of system collections
     *
     * @return array
     */
    public function getSystemCollections() {
        return $this->settings['settings']['systemCollections'];
    }

    /**
     * Return audio or video
     *
     * @return string
     */
    public function getMediatype() {
        return $this->settings['settings']['mediatype'] == null ? 'Audio' : $this->settings['settings']['mediatype'];
    }

    /**
     * Returns whether the download-option shouldn't be visible
     *
     * @return bool
     */
    public function getHideOptions() {
        return $this->settings['settings']['hideOptions'] == null ? false : $this->settings['settings']['hideOptions'];
    }

    /**
     * Returns whether the cover (audio only) shouldn't be visible
     *
     * @return bool
     */
    public function getHideCover() {
        return $this->settings['settings']['hideCover'] == null ? false : $this->settings['settings']['hideCover'];
    }

    /**
     * Return default audio cover
     *
     * @return string
     */
    public function getAudioCover() {
        return $this->settings['settings']['audioCover'];
    }

    /**
     * Return default video cover
     *
     * @return string
     */
    public function getVideoCover() {
        return $this->settings['settings']['videoCover'];
    }

    /**
     * Return horizontal or vertical orientation
     *
     * @return string
     */
    public function getOrientation() {
        return $this->settings['settings']['orientation'];
    }

    /**
     * Return max player width
     *
     * @return string
     */
    public function getMaxWidth() {
        return $this->settings['settings']['maxWidth'] == null ? 'auto' : $this->settings['settings']['maxWidth'] . $this->settings['settings']['maxWidthUnit'];
    }

    /**
     * Return source type
     *
     * @return string
     */
    public function getSource() {
        return $this->settings['settings']['source'];
    }

    /**
     * Return limit of mediaitems
     *
     * @return integer
     */
    public function getLimit() {
        return $this->settings['settings']['limit'];
    }

    /**
     * Return the group-criteria of mediaitems
     *
     * @return string
     */
    public function getGroupBy() {
        return $this->settings['settings']['groupBy'];
    }

    /**
     * Return order of mediaitems
     *
     * @return string
     */
    public function getOrderBy() {
        return $this->settings['settings']['orderBy'];
    }

    /**
     * Return direction of order by
     *
     * @return string
     */
    public function getOrderByDirection() {
        return $this->settings['settings']['orderByDirection'];
    }

    /**
     * Return whether group headlines should be hidden
     *
     * @return bool
     */
    public function getHideGroupHeadlines() {
        return $this->settings['settings']['hideGroupHeadlines'] == null ? false : $this->settings['settings']['hideGroupHeadlines'];
    }

    /**
     * Returns whether the playlist should be hidden
     *
     * @return bool
     */
    public function getHidePlaylist() {
        return $this->settings['settings']['hidePlaylist'] == null ? false : $this->settings['settings']['hidePlaylist'];
    }

    /**
     * Inject settings via ConfigurationManager.
     *
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager) {
        $this->settings = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
    }

    /**
     * @param $dumpData
     */
    public function dump($dumpData) {
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($dumpData);
    }
}
