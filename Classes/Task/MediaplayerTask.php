<?php

namespace Auxiliaweb\AuxiliawebMediaplayer\Task;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * MediaplayerTask
 */
class MediaplayerTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {

    /**
     * action show
     *
     * @return bool
     */
    public function execute() {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $indexer = $objectManager->get(\Auxiliaweb\AuxiliawebMediaplayer\Service\MediaplayerService::class);
        $indexer->indexMediaitems(
            $this->auxiliaweb_mediaplayer_path,
            $this->auxiliaweb_mediaplayer_recursive,
            $this->auxiliaweb_mediaplayer_storagePid,
            $this->auxiliaweb_mediaplayer_systemCollectionUid
        );

        return true;
    }
}
