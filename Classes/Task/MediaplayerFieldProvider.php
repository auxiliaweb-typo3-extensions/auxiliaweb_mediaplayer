<?php

namespace Auxiliaweb\AuxiliawebMediaplayer\Task;

use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/***
 *
 * This file is part of the "Mediaplayer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * MediaplayerFieldProvider
 */
class MediaplayerFieldProvider implements \TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface {

    const PREFIX = 'auxiliaweb_mediaplayer_';

    /**
     * Array with fields for schedular-form
     *
     * @var array
     */
    protected $fields = [
        'path' => [
            'type' => 'string',
            'default' => '',
            'placeholder' => 'fileadmin/user_upload/mediaplayer/',
            'required' => true
        ],
        'recursive' => [
            'type' => 'bool',
            'default' => false,
            'required' => false
        ],
        'storagePid' => [
            'type' => 'string',
            'default' => '0',
            'placeholder' => '0',
            'required' => false
        ],
        'systemCollectionUid' => [
            'type' => 'string',
            'default' => '0',
            'placeholder' => '0',
            'required' => false
        ],
    ];

    /**
     * @param array $taskInfo
     * @param $task
     * @param SchedulerModuleController $parentObject
     * @return array
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $parentObject) {
        $additionalFields = [];

        foreach ($this->fields as $key => $field) {
            $fieldKey = self::PREFIX . $key;
            $additionalFields[$fieldKey] = [
                'code' => $this->createFieldHtml($fieldKey, $field, $task, $taskInfo, $parentObject),
                'label' => LocalizationUtility::translate('tx_auxiliawebmediaplayer_mediaplayer_scheduler.' . $key, 'auxiliaweb_mediaplayer'),
                'cshKey' => '_MOD_auxiliaweb_mediaplayer_scheduler',
                'cshLabel' => $key
            ];
        }

        return $additionalFields;
    }

    /**
     * @param array $submittedData
     * @param SchedulerModuleController $parentObject
     * @return bool
     */
    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $parentObject) {
        $errorExists = false;
        foreach ($this->fields as $key => $field) {
            $value = trim($submittedData[self::PREFIX . $key]);
            if ($field['required'] === true && empty(self::PREFIX . $key)) {
                $errorExists = true;
                $parentObject->addMessage('Field: ' . $key . ' can not be empty', FlashMessage::ERROR);
            } else {
                $submittedData[self::PREFIX . $key] = $value;
            }
        }
        return !$errorExists;
    }

    /**
     * @param array $submittedData
     * @param AbstractTask $task
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task) {
        foreach ($this->fields as $key => $field) {
            $fieldKey = self::PREFIX . $key;
            $task->$fieldKey = $submittedData[$fieldKey];
        }
    }

    /**
     * @param $key
     * @param $field
     * @param $task
     * @param array $taskInfo
     * @return string
     */
    private function createFieldHtml($key, $field, $task, array &$taskInfo, SchedulerModuleController $parentObject) {
        $textInput = '<input type="text" class="form-control" name="tx_scheduler[%s]" id="%s" value="%s" placeholder="%s" />';
        $checkboxInput = '<input type="checkbox" class="checkbox" name="tx_scheduler[%s]" id="%s" %s value="1"/>';
        if ($field['type'] === 'string') {
            return sprintf($textInput, $key, $key, $this->getValue($key, $field, $task, $taskInfo, $parentObject), isset($field['placeholder']) ? $field['placeholder'] : '');
        } else if ($field['type'] === 'bool') {
            return sprintf($checkboxInput, $key, $key, $this->getValue($key, $field, $task, $taskInfo, $parentObject) ? 'checked="checked"' : '');
        }
    }

    /**
     * @param $key
     * @param $field
     * @param $task
     * @param array $taskInfo
     * @return mixed
     */
    private function getValue($key, $field, $task, array &$taskInfo, SchedulerModuleController $parentObject) {
        if (empty($taskInfo[$key])) {
            if ($parentObject->CMD === 'add' && isset($field['default'])) {
                return $field['default'];
            }
            if ($parentObject->CMD == 'edit') {
                return $task->$key;
            }
        } else {
            return $taskInfo[$key];
        }
    }
}