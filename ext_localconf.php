<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Auxiliaweb.AuxiliawebMediaplayer',
            'Mediaplayer',
            [
                'Mediaplayer' => 'show'
            ],
            // non-cacheable actions
            [
                'Mediaplayer' => '',
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        player {
                            iconIdentifier = auxiliaweb_mediaplayer-plugin-mediaplayer
                            title = LLL:EXT:auxiliaweb_mediaplayer/Resources/Private/Language/locallang_db.xlf:tx_auxiliawebmediaplayer_mediaplayer.name
                            description = LLL:EXT:auxiliaweb_mediaplayer/Resources/Private/Language/locallang_db.xlf:tx_auxiliawebmediaplayer_mediaplayer.description
                            tt_content_defValues {
                                CType = list
                                list_type = auxiliawebmediaplayer_mediaplayer
                            }
                        }
                    }
                    show = *
                }
           }'
        );
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'auxiliaweb_mediaplayer-plugin-mediaplayer',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:auxiliaweb_mediaplayer/Resources/Public/Icons/icons8-lautsprecher-64.png']
        );

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Auxiliaweb\AuxiliawebMediaplayer\Task\MediaplayerTask::class] = array(
            'extension' => 'auxiliaweb_mediaplayer',
            'title' => 'LLL:EXT:auxiliaweb_mediaplayer/Resources/Private/Language/locallang_db.xlf:tx_auxiliawebmediaplayer_mediaplayer.name',
            'description' => 'LLL:EXT:auxiliaweb_mediaplayer/Resources/Private/Language/locallang_db.xlf:tx_auxiliawebmediaplayer_mediaplayer.name',
            'additionalFields' => \Auxiliaweb\AuxiliawebMediaplayer\Task\MediaplayerFieldProvider::class
        );
    }
);
