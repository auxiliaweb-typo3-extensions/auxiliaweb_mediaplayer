<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['auxiliawebmediaplayer_mediaplayer'] = 'recursive,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['auxiliawebmediaplayer_mediaplayer'] = 'pi_flexform';
