
plugin.tx_auxiliawebmediaplayer {
    view {
        templateRootPaths.0 = EXT:auxiliaweb_mediaplayer/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_auxiliawebmediaplayer.view.templateRootPath}
        partialRootPaths.0 = EXT:auxiliaweb_mediaplayer/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_auxiliawebmediaplayer.view.partialRootPath}
        layoutRootPaths.0 = EXT:auxiliaweb_mediaplayer/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_auxiliawebmediaplayer.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_auxiliawebmediaplayer.persistence.storagePid}
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        oneMediaplayerPerMediaitem = {$plugin.tx_auxiliawebmediaplayer.settings.oneMediaplayerPerMediaitem}
        mediatype = {$plugin.tx_auxiliawebmediaplayer.settings.mediatype}
        hideOptions = {$plugin.tx_auxiliawebmediaplayer.settings.hideOptions}
        hideCover = {$plugin.tx_auxiliawebmediaplayer.settings.hideCover}
        audioCover = {$plugin.tx_auxiliawebmediaplayer.settings.audioCover}
        orientation = {$plugin.tx_auxiliawebmediaplayer.settings.orientation}
        maxWidth = {$plugin.tx_auxiliawebmediaplayer.settings.maxWidth}
        maxWidthUnit = {$plugin.tx_auxiliawebmediaplayer.settings.maxWidthUnit}
        source = {$plugin.tx_auxiliawebmediaplayer.settings.source}
        limit = {$plugin.tx_auxiliawebmediaplayer.settings.limit}
        orderBy = {$plugin.tx_auxiliawebmediaplayer.settings.orderBy}
        groupBy = {$plugin.tx_auxiliawebmediaplayer.settings.groupBy}
        orderByDirection = {$plugin.tx_auxiliawebmediaplayer.settings.orderByDirection}
        hideGroupHeadlines = {$plugin.tx_auxiliawebmediaplayer.settings.hideGroupHeadlines}
        hidePlaylist = {$plugin.tx_auxiliawebmediaplayer.settings.hidePlaylist}
    }
}

page.includeCSS {
    auxiliaweb_mediaplayer = {$plugin.tx_auxiliawebmediaplayer.resources.css}
}

page.includeJSFooter {
    auxiliaweb_mediaplayer = {$plugin.tx_auxiliawebmediaplayer.resources.js}
}
