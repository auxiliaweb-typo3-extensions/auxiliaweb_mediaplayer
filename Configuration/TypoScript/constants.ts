
plugin.tx_auxiliawebmediaplayer {
    view {
        # cat=plugin.tx_auxiliawebmediaplayer/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:auxiliaweb_mediaplayer/Resources/Private/Templates/
        # cat=plugin.tx_auxiliawebmediaplayer/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:auxiliaweb_mediaplayer/Resources/Private/Partials/
        # cat=plugin.tx_auxiliawebmediaplayer/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:auxiliaweb_mediaplayer/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_auxiliawebmediaplayer/a; type=string; label=Default storage PID
        storagePid =
    }
    resources {
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Videocover
        css = EXT:auxiliaweb_mediaplayer/Resources/Public/CSS/mediaplayer.css
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Videocover
        js = EXT:auxiliaweb_mediaplayer/Resources/Public/Javascript/mediaplayer.js
    }
    settings {
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=boolean; label=Show one mediaplayer per mediaitem
        oneMediaplayerPerMediaitem =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Mediatype
        mediatype =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=boolean; label=Hide download-option
        hideOptions =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=boolean; label=Hide cover (audio only)
        hideCover =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Audiocover
        audioCover = typo3conf/ext/auxiliaweb_mediaplayer/Resources/Public/Icons/audio.jpg
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Orientation (horizontal / vertical)
        orientation =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Max mediaplayer width
        maxWidth =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Max mediaplayer width unit (px, % ...)
        maxWidthUnit =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Should the plugin use single mediaitems or system collections?
        source =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Limit of shown mediaitems
        limit =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Group mediaitems by ...
        groupBy =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Order mediaitems by ...
        orderBy =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=string; label=Ascending/Descendin
        orderByDirection =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=boolean; label=Hide group headlines
        hideGroupHeadlines =
        # cat = plugin.tx_auxiliawebmediaplayer/a; type=boolean; label=Hide playlist
        hidePlaylist =
    }
}
