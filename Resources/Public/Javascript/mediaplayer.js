(function () {
    let sliding = [];
    let playlist = [];
    let currentTrack = [];

    document.addEventListener('DOMContentLoaded', function (event) {
        let mediaplayerList = document.querySelectorAll('.mediaplayer');
        for (let mediaplayer of mediaplayerList) {
            if (mediaplayer.getAttribute('class').indexOf('audio') > -1) {
                initAudioplayer(mediaplayer);
            }
        }
    });

    function initAudioplayer(mediaplayer) {
        // elements
        let audioElement = mediaplayer.querySelector('audio');

        let rewindElement = mediaplayer.querySelector('.mediaplayer-rewind');
        let previousElement = mediaplayer.querySelector('.mediaplayer-previous');
        let playButtonElement = mediaplayer.querySelector('.mediaplayer-play');
        let pauseButtonElement = mediaplayer.querySelector('.mediaplayer-pause');
        let nextElement = mediaplayer.querySelector('.mediaplayer-next');
        let fastForwardElement = mediaplayer.querySelector('.mediaplayer-fast-forward');

        let currentTimeElement = mediaplayer.querySelector('.mediaplayer-current-time');
        let durationElement = mediaplayer.querySelector('.mediaplayer-duration');
        let progressbarElement = mediaplayer.querySelector('.mediaplayer-progressbar');

        let currentTitleElement = mediaplayer.querySelector('.mediaplayer-current-title');
        let currentInterpreterElement = mediaplayer.querySelector('.mediaplayer-current-interpreter');
        let currentReleaseDateElement = mediaplayer.querySelector('.mediaplayer-current-release-date');
        let currentCoverImageElement = mediaplayer.querySelector('.mediaplayer-current-cover-image');

        // rest
        sliding[audioElement.id] = false;
        playlist[audioElement.id] = [];

        if (currentCoverImageElement.getAttribute('data-mediaplayer-hide-cover') === '' || currentCoverImageElement.getAttribute('data-mediaplayer-hide-cover') === '0') {
            currentCoverImageElement.classList.remove('mediaplayer-hide-element');
        }

        // init items
        for (let mediaitem of mediaplayer.querySelectorAll('.mediaplayer-playlist > li')) {
            const item = {
                title: mediaitem.getAttribute('data-mediaitem-title'),
                interpreter: mediaitem.getAttribute('data-mediaitem-interpreter'),
                releaseDate: mediaitem.getAttribute('data-mediaitem-release-date'),
                src: mediaitem.getAttribute('data-mediaitem-src'),
                cover: mediaitem.getAttribute('data-mediaitem-cover'),
            };

            playlist[audioElement.id].push(item);

            // init mediaitem controls
            mediaitem.querySelector('.mediaitem-play').onclick = function () {
                console.log(playlist[audioElement.id]);
                nextTrack(audioElement, playlist[audioElement.id].indexOf(item), currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
                audioElement.play();
            };

            let displayOptionsElement = mediaitem.querySelector('.mediaitem-display-options');
            let optionsElement = mediaitem.querySelector('.mediaitem-options');
            let mediaitemDescriptionWrapperElement = mediaitem.querySelector('.mediaitem-description');
            let coverElement = mediaitem.querySelector('.mediaitem-cover');

            if (displayOptionsElement) {
                mediaitem.querySelector('.mediaitem-display-options').onclick = function () {
                    if (mediaitemDescriptionWrapperElement.classList.contains('mediaitem-hide-description')) {
                        mediaitemDescriptionWrapperElement.classList.remove('mediaitem-hide-description');
                        displayOptionsElement.classList.remove('mediaitem-options-active');
                        optionsElement.classList.remove('mediaitem-options-visible');
                        coverElement.classList.remove('mediaitem-hide-cover');
                    } else {
                        mediaitemDescriptionWrapperElement.classList.add('mediaitem-hide-description');
                        displayOptionsElement.classList.add('mediaitem-options-active');
                        optionsElement.classList.add('mediaitem-options-visible');
                        coverElement.classList.add('mediaitem-hide-cover');
                    }
                };
            }
        }

        // init mediaplayer controls
        // Rwind
        rewindElement.onclick = function () {
            audioElement.currentTime -= 15;
        };
        // Previous
        previousElement.onclick = function () {
            const paused = audioElement.paused;
            if (playlist[audioElement.id][0] === currentTrack[audioElement.id]) {
                nextTrack(audioElement, playlist[audioElement.id].length - 1, currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
            } else {
                nextTrack(audioElement, playlist[audioElement.id].indexOf(currentTrack[audioElement.id]) - 1, currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
            }
            if (!paused) {
                audioElement.play();
            }
        };
        // Play
        playButtonElement.onclick = function () {
            audioElement.play();
        };
        // Pause
        pauseButtonElement.onclick = function () {
            audioElement.pause();
        };
        // Next
        nextElement.onclick = function () {
            const paused = audioElement.paused;
            if (playlist[audioElement.id][playlist[audioElement.id].length - 1] === currentTrack[audioElement.id]) {
                nextTrack(audioElement, 0, currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
            } else {
                nextTrack(audioElement, playlist[audioElement.id].indexOf(currentTrack[audioElement.id]) + 1, currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
            }
            if (!paused) {
                audioElement.play();
            }
        };
        // Fast Forward
        fastForwardElement.onclick = function () {
            audioElement.currentTime += 15;
        };
        // Progressbar Changes committed
        progressbarElement.onchange = function () {
            audioElement.currentTime = (audioElement.duration * progressbarElement.value) / 100;
            sliding[audioElement.id] = false;
        };
        // Sliding on progressbar
        progressbarElement.oninput = function () {
            sliding[audioElement.id] = true;
        };
        // Audio starts to play
        audioElement.onplay = function () {
            playButtonElement.classList.add('mediaplayer-hide-element');
            pauseButtonElement.classList.remove('mediaplayer-hide-element');
        };
        // Audio paused
        audioElement.onpause = function () {
            playButtonElement.classList.remove('mediaplayer-hide-element');
            pauseButtonElement.classList.add('mediaplayer-hide-element');
        };
        // Audio finished
        audioElement.onended = function () {
            if (playlist[audioElement.id][playlist[audioElement.id].length - 1] === currentTrack[audioElement.id]) {
                playButtonElement.classList.remove('mediaplayer-hide-element');
                pauseButtonElement.classList.add('mediaplayer-hide-element');
            } else {
                nextTrack(audioElement, playlist[audioElement.id].indexOf(currentTrack[audioElement.id]) + 1, currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
                audioElement.play();
            }
        };

        if (playlist[audioElement.id].length > 0) {
            nextTrack(audioElement, 0, currentTitleElement, currentInterpreterElement, currentCoverImageElement, currentReleaseDateElement);
        }

        setInterval(setProgress, 100, audioElement, currentTimeElement, durationElement, progressbarElement);
    }

    function nextTrack(audioElement, nextIndex, currentTitleElement, currentInterpreterElement, currentCoverImage, currentReleaseDateElement) {
        currentTrack[audioElement.id] = playlist[audioElement.id][nextIndex];
        audioElement.src = currentTrack[audioElement.id].src;
        currentTitleElement.innerHTML = currentTrack[audioElement.id].title;
        currentInterpreterElement.innerHTML = currentTrack[audioElement.id].interpreter;
        currentReleaseDateElement.innerHTML = currentTrack[audioElement.id].releaseDate;
        currentCoverImage.querySelector('img').src = currentTrack[audioElement.id].cover;
    }

    function setProgress(audioElement, currentTimeElement, durationElement, progressbarElement) {
        currentTimeElement.innerHTML = formatTime(audioElement.currentTime);
        durationElement.innerHTML = formatTime(audioElement.duration);
        if (!sliding[audioElement.id]) {
            progressbarElement.value = (audioElement.currentTime / audioElement.duration) * 100;
        }
    }

    function formatTime(time) {
        let minutes = Math.floor(time / 60),
            seconds = Math.floor((time - minutes * 60));

        return (minutes.toString().length === 1 ? '0' : '') + minutes
            + ':'
            + (seconds.toString().length === 1 ? '0' : '') + seconds
    }
})();
